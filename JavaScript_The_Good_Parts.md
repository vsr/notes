# JavaScript: The Good Parts #



## Good Parts  ##

### Good Ideas ###

* functions
* loose typing
* dynamic objects
* expressive object literal notation


### Bad Ideas ###

* global variables \#badpart


### misc ###

* first class objects with *(mostly) lexical scoping*
* class free object system, *prototypal inheritance*
* lightweight, expressive


## Grammar ##


### components ###

* Whitespace - formatting characters(tab, space, line end) or comments(block, inline).
* Names - a letter optionally followed by one or more letters, digits, underscores.
* Names cannot be any of these reserved words

        abstract
        boolean break byte
        case catch char class const continue
        debugger default delete do double
        else enum export extends
        false final finally float for function
        goto
        if implements import in instanceof int interface
        long
        native new null
        package private protected public
        return
        short static super switch synchronized
        this throw throws transient true try typeof
        var volatile void
        while with

* `undefined`, `NaN`, and `Infinity` should have been reserved but were not. \#badpart
* Reserved words cannot be used for: `variable`, `parameter`, `object property` names. \#badpart
* Single number type- represented by 64-bit floating point number.



### misc ###

* Recommended to avoid *block comments* since those symbols `/* */` can also occur in regex literals. Use *line comments* `//`.
* `NaN` is not equal to anything, not even itself.
* Infinity represents all values greater than 1.79769313486231570e+308





